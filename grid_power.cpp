#include <iostream>
#include <cstdio>
#include <complex>

#include <boost/program_options.hpp>

using namespace std;
using namespace boost::program_options;

static int read(const char filename[], vector<complex<double>>& v);
static void init_mas_corretion(const int nc, const int n_mas,
			       vector<double>& v);

int main(int argc, char* argv[])
{
  options_description opt("grid_power [options] <delta(k) binary file>");
  opt.add_options()
    ("help,h", "display this help")
    ("n-mas", value<int>()->default_value(0), "index of mass assignment scheme; NGP=1, CIC=2, TSC=3")
    ("filename", value<string>(), "grid binary data")
    ;

  positional_options_description p;
  p.add("filename", -1);

  variables_map vm;
  store(command_line_parser(argc, argv).options(opt).positional(p).run(), vm);
  notify(vm);

  if(vm.count("help") || ! vm.count("filename")) {
    cout << opt;
    return 0;
  }

  const string filename = vm["filename"].as<string>();
  const int n_mas= vm["n-mas"].as<int>();

  vector<complex<double>> v;
  const int nc= read(filename.c_str(), v);

  vector<double> vmas;
  init_mas_corretion(nc, n_mas, vmas);

  const int knq= nc/2;
  const size_t nckz= nc/2 + 1;

  for(int ix=0; ix<nc; ++ix) {
    int kx = ix <= knq ? ix : ix - nc;
    double corr_x = vmas[ix];
      
    for(int iy=0; iy<nc; ++iy) {
      int ky = iy <= knq ? iy : iy - nc;
      double corr_xy = corr_x * vmas[iy];
	
      for(int kz=0; kz<knq; ++kz) {
	size_t index= nckz*(nc*ix + iy) + kz;
	double corr_xyz = corr_xy * vmas[kz];
	    
	printf("%d %d %d %.15le %.15le\n", kx, ky, kz,
	       v[index].real()*corr_xyz,
	       v[index].imag()*corr_xyz);

      }
    }
  }

  return 0;
}

int read(const char filename[], vector<complex<double>>& v)
{
  int nc;
  FILE* fp= fopen(filename, "r");
  if(fp == 0) {
    cerr << "Error: unable to open file, " << filename << endl;
    throw filename;
  }


  fread(&nc, sizeof(int), 1, fp);
  const size_t nckz= nc/2 + 1;
  v.resize(nc*nc*nckz);

  size_t ret= fread(v.data(), sizeof(complex<double>), nc*nc*nckz, fp);
  assert(ret == nc*nc*nckz);


  int nc_check;
  ret= fread(&nc_check, sizeof(int), 1, fp);
  assert(ret == 1);
  cerr << nc_check << endl;
  assert(nc_check == nc);
  
  fclose(fp);
  return nc;
}

void init_mas_corretion(const int nc, const int n_mas,
			vector<double>& v) {
  v.reserve(nc);
  v.assign(nc, 1.0);

  if(n_mas == 0) // i.e. when MAS_correction = false
    return;
  
  const int knq = nc/2;
  const double fac= M_PI/nc;

  for(int i=1; i<nc; ++i) {
    int k= i <= knq ? i : i - nc;
    double sinc = sin(fac*k)/(fac*k);
    v[i] = 1.0/pow(sinc, 2*n_mas);
  }

  assert(v.size() == static_cast<size_t>(nc));
}
