# grid_power

Read delta(k) grid data and print delta(k) after windown function correction.

```
grid_power --n-mas=2 grid.b
```

Output is

```
Column 1: kx (integer)  in units of fundamental frequency
Column 2: ky (integer)
Column 3: kz (integer)
Column 4: Re(delta(k))
Column 5: Im(delta(k))
```

The amplitude of delta(k) is such that the power spectrum is

```
P(K) = Lbox^3/Np^2 <|delta(k)|^2>
```

where Lbox is the length of the box and Np is number of particles.

