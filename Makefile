EXEC = grid_power
all: $(EXEC)

# Define your compiler
CXX := c++ -std=c++11

CPPFLAGS := -I.. $(OPENMP) -O3 


BOOST_DIR ?=

DIR_PATH   = $(BOOST_DIR)

# Headers and libraries are assumed under /include and /lib directories respectively
# under the location above
CPPFLAGS  += $(foreach dir, $(DIR_PATH), -I$(dir)/include)
LIBS      += $(foreach dir, $(DIR_PATH), -L$(dir)/lib)
LIBS      += -lfftw3 -lboost_program_options


OBJS := grid_power.o

grid_power: $(OBJS)
	$(CXX) $(OBJS) $(LIBS) -o $@


.PHONY: clean dependence

clean:
	rm -f $(EXEC) $(OBJS)

